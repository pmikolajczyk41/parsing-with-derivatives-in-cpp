#!/usr/bin/env bash

cmake --version

mkdir -p build
mkdir -p out
cd build
cmake ..
make
cd ..
mv build/Derivatives out/