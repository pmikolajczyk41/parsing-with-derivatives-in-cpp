#define CATCH_CONFIG_MAIN

#include "TestUtils.hpp"
#include <list>

using namespace tests;

#ifdef USE_CONCEPTS

TEST_CASE("alphabet concept", "[concepts]") {
    static_assert(Alphabet<AsciiAlphabet>);
    static_assert(!Alphabet<int>);
    static_assert(Alphabet<IntAlphabet>);
}

TEST_CASE("int alphabet concept", "[concepts]") {
    Token<IntAlphabet> two{2};
    Alternation<IntAlphabet> alternation(two, *(new Epsilon<IntAlphabet>{}));

    std::list<int> two_word = {2};
    std::list<int> fourty_word = {40};

    Parser<IntAlphabet> parser{};
    REQUIRE(parser.doesContain(alternation, two_word));
    REQUIRE_FALSE(parser.doesContain(alternation, fourty_word));
}

#endif

TEST_CASE("atomic", "[recognizer][regular]") {
    REQUIRE(contains(epsilon, std::string("")));
    REQUIRE_FALSE(contains(epsilon, std::string("aa")));

    REQUIRE_FALSE(contains(empty, std::string("")));
    REQUIRE_FALSE(contains(empty, std::string("b")));

    REQUIRE(contains(a, std::string("a")));
    REQUIRE_FALSE(contains(a, std::string("c")));
}

TEST_CASE("simple complex", "[recognizer][regular]") {
    Concatenation<AsciiAlphabet> grammar(epsilon, empty);
    Alternation<AsciiAlphabet> grammar2(empty, epsilon);

    REQUIRE_FALSE(contains(grammar, std::string("")));
    REQUIRE(contains(grammar2, std::string("")));
    REQUIRE_FALSE(contains(grammar2, std::string("a")));
}

TEST_CASE("cc*", "[recognizer][regular]") {
    auto* grammar = grammars::ccStar();

    REQUIRE(contains(*grammar, std::string("cc")));
    REQUIRE(contains(*grammar, std::string("cccc")));
    REQUIRE_FALSE(contains(*grammar, std::string("")));
    REQUIRE_FALSE(contains(*grammar, std::string("cca")));
}

TEST_CASE("some words", "[recognizer][regular]") {
    //L -> aaa | aba | caca | b
    auto& grammar = sequence<AsciiAlphabet>(std::string("aaa")) | sequence<AsciiAlphabet>(std::string("aba")) |
                   sequence<AsciiAlphabet>(std::string("caca")) |
                   sequence<AsciiAlphabet>(std::string("b"));

    REQUIRE(contains(grammar, std::string("aba")));
    REQUIRE(contains(grammar, std::string("caca")));
    REQUIRE_FALSE(contains(grammar, std::string("cac")));
    REQUIRE_FALSE(contains(grammar, std::string("bb")));
}

TEST_CASE("a^n b^n", "[recognizer][non-regular]") {
    auto* grammar = grammars::aNbN();

    REQUIRE(contains(*grammar, std::string("ab")));
    REQUIRE(contains(*grammar, std::string("")));
    REQUIRE_FALSE(contains(*grammar, std::string("aab")));
    REQUIRE(contains(*grammar, std::string("aaaaabbbbb")));
}

TEST_CASE("nested () and []", "[recognizer][non-regular]") {
    auto* grammar = grammars::nestedParentheses();

    REQUIRE(contains(*grammar, std::string("")));
    REQUIRE(contains(*grammar, std::string("()")));
    REQUIRE(contains(*grammar, std::string("[]")));
    REQUIRE(contains(*grammar, std::string("([])")));
    REQUIRE(contains(*grammar, std::string("([[[()()[][]]]([])])")));
    REQUIRE_FALSE(contains(*grammar, std::string("([[[")));
    REQUIRE_FALSE(contains(*grammar, std::string("[)")));
}

TEST_CASE("once settable children", "[once settable]") {
    std::string error = "Value already set";

    Alternation<AsciiAlphabet> alternation{nullptr, nullptr};
    REQUIRE_NOTHROW(alternation.setLeftChild(a));
    REQUIRE_THROWS_WITH(alternation.setLeftChild(a), error);
    REQUIRE_THROWS_WITH(alternation.setLeftChild(b), error);
}

TEST_CASE("once settable nullable", "[once settable]") {
    std::string error = "Value already set";

    Alternation<AsciiAlphabet> alternation{nullptr, nullptr};
    REQUIRE_NOTHROW(alternation.setNullableState(NOT_NULLABLE));
    REQUIRE_THROWS_WITH(alternation.setNullableState(NOT_NULLABLE), error);

    REQUIRE_THROWS_WITH(epsilon.setNullableState(NULLABLE), error);
    REQUIRE_THROWS_WITH(epsilon.setNullableState(NOT_NULLABLE), error);
    REQUIRE_THROWS_WITH(empty.setNullableState(NOT_NULLABLE), error);
    REQUIRE_THROWS_WITH(a.setNullableState(NOT_NULLABLE), error);
}

TEST_CASE("only child", "[complete initialization]") {
    Concatenation<AsciiAlphabet> grammar(epsilon, nullptr);
    REQUIRE_THROWS_WITH(grammar.takeDerivative('a'), "Uninitialized child");
}