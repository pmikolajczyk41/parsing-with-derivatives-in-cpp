#include <WrappedGrammar.hpp>
#include "TestUtils.hpp"

namespace grammars {
    using namespace tests;

    const Node<AsciiAlphabet>* aNbN() {
        //L -> aLb | eps
        //
        //L -> A | eps
        //A -> a.B
        //B -> L.b
        auto* grammar = new Alternation<AsciiAlphabet>(nullptr, epsilon);
        auto* B = new Concatenation<AsciiAlphabet>(*grammar, b);
        auto* A = new Concatenation<AsciiAlphabet>(a, *B);
        grammar->setLeftChild(*A);
        return grammar;
    }

    const Node<IntAlphabet>* aNbN_marked() {
        //L -> aLb | eps
        productions_t P{
                {{{TERMINAL, 1}, {NONTERMINAL, 0}, {TERMINAL, 2}}, {EpsilonSymbol<>()}}
        };
        return (new WrappedGrammar(P))->getRoot();
    }

    const Node<AsciiAlphabet>* nestedParentheses() {
        //L -> eps | LL | [L] | (L)
        //
        //L -> eps | L1
        //L1 -> Double | L2
        //  Double -> L.L
        //L2 -> Brackets | Parentheses
        //  Brackets -> [.Brackets1
        //      Brackets1 -> L.]
        //  Parentheses -> (.Parentheses1
        //      Parentheses1 -> L.)
        auto* grammar = new Alternation<AsciiAlphabet>(epsilon, nullptr);

        auto* Brackets1 = new Concatenation<AsciiAlphabet>(*grammar, rightBracket);
        auto* Brackets = new Concatenation<AsciiAlphabet>(leftBracket, *Brackets1);

        auto* Parentheses1 = new Concatenation<AsciiAlphabet>(*grammar, rightPar);
        auto* Parentheses = new Concatenation<AsciiAlphabet>(leftPar, *Parentheses1);
        auto* Double = new Concatenation<AsciiAlphabet>(*grammar, *grammar);

        auto* L2 = new Alternation<AsciiAlphabet>(*Brackets, *Parentheses);
        auto* L1 = new Alternation<AsciiAlphabet>(*Double, *L2);

        grammar->setRightChild(*L1);
        return grammar;
    }

    const Node<IntAlphabet>* nestedParentheses_marked() {
        //L -> eps | D | SQ | BR
        //D -> L.L
        //SQ -> [.L.]
        //BR -> (.L.)

        productions_t P{
                {{EpsilonSymbol<>()}, {{NONTERMINAL, 1}}, {{NONTERMINAL, 2}}, {{NONTERMINAL, 3}}},
                {{{NONTERMINAL, 0}, {NONTERMINAL, 0}}},
                {{{TERMINAL,    1}, {NONTERMINAL, 0}, {TERMINAL, 2}}},
                {{{TERMINAL,    3}, {NONTERMINAL, 0}, {TERMINAL, 4}}},
        };
        return (new WrappedGrammar(P))->getRoot();
    }

    const Node<IntAlphabet>* catalan_marked() {
        //L -> a | L.L

        productions_t P{
                {{{TERMINAL, 1}}, {{NONTERMINAL, 0}, {NONTERMINAL, 0}}}
        };
        return (new WrappedGrammar(P))->getRoot();
    }

    const Node<AsciiAlphabet>* ccStar() {
        //L -> (L.c) | c
        //
        //L -> A | c
        //A -> L.c
        auto* grammar = new Alternation<AsciiAlphabet>(nullptr, c);
        auto* A = new Concatenation<AsciiAlphabet>(*grammar, c);
        grammar->setLeftChild(*A);
        return grammar;
    }

    const Node<IntAlphabet>* ccStar_marked() {
        //L -> (L.c) | c
        productions_t P{
                {{{NONTERMINAL, 0}, {TERMINAL, 3}}, {{TERMINAL, 3}}}
        };
        return (new WrappedGrammar(P))->getRoot();
    }

    const Node<AsciiAlphabet>* ThreeSameWords() {
        //L -> aa | aa | aa
        //
        //L -> A | B
        //A -> C | D
        //B -> a.a
        //C -> a.a
        //D -> a.a
        auto* B = new Concatenation<AsciiAlphabet>(a, a);
        auto* C = new Concatenation<AsciiAlphabet>(a, a);
        auto* D = new Concatenation<AsciiAlphabet>(a, a);
        auto* A = new Alternation<AsciiAlphabet>(*C, *D);
        return new Alternation<AsciiAlphabet>(*A, *B);
    }

    const Node<AsciiAlphabet>* ABCInEightOptions() {
        //L -> (a | a) . (b | b) . (c | c)
        //
        //L -> As . BC
        //As -> a | a
        //BC -> Bs . Cs
        //Bs -> b | b
        //Cs -> c | c
        auto* As = new Alternation<AsciiAlphabet>(a, a);
        auto* Bs = new Alternation<AsciiAlphabet>(b, b);
        auto* Cs = new Alternation<AsciiAlphabet>(c, c);
        auto* BC = new Concatenation<AsciiAlphabet>(*Bs, *Cs);
        return new Concatenation<AsciiAlphabet>(*As, *BC);
    }

    const Node<IntAlphabet>* ABCInEightOptions_marked() {
        //L -> A . B . C
        //A -> a | a
        //B -> b | b
        //C -> c | c
        productions_t P{
                {{{NONTERMINAL, 1}, {NONTERMINAL, 2}, {NONTERMINAL, 3}}},
                {{{TERMINAL,    1}}, {{TERMINAL, 1}}},
                {{{TERMINAL,    2}}, {{TERMINAL, 2}}},
                {{{TERMINAL,    3}}, {{TERMINAL, 3}}}
        };
        return (new WrappedGrammar(P))->getRoot();
    }
}


namespace tests {
    bool hasSuchParseTree(const Node<IntAlphabet>& grammar, const std::vector<int>& word,
                          const std::vector<int>& parseTree) {
        std::vector<int> result;
        Parser<IntAlphabet>{}.parse(grammar, word).traverseTree(result);
        return result == parseTree;
    }
}