#include "TestUtils.hpp"

#ifdef USE_CONCEPTS

TEST_CASE("semiring concept", "[concepts]") {
    static_assert(Semiring<BooleanSemiring>);
    static_assert(!Semiring<bool>);
    static_assert(Semiring<IntegerSemiring>);
    static_assert(!Semiring<int>);
    static_assert(Semiring<ParseTreeSemiring<char>>);
    static_assert(!Semiring<std::string>);
}

#endif

using namespace tests;

TEST_CASE("counting parse trees in atomic", "[counting, unambiguous]") {
    REQUIRE(hasNoTrees(empty, std::string("")));
    REQUIRE(hasNoTrees(epsilon, std::string("a")));
    REQUIRE(hasUniqueTree(epsilon, std::string("")));
    REQUIRE(hasUniqueTree(a, std::string("a")));
    REQUIRE(hasNoTrees(a, std::string("b")));
    REQUIRE(hasUniqueTree(parsedA, std::string("")));
    REQUIRE_FALSE(hasInfTrees(a, std::string("a")));
    REQUIRE_FALSE(hasInfTrees(parsedA, std::string("b")));
}

TEST_CASE("counting in unambiguous", "[counting, unambiguous]") {
    REQUIRE(hasNoTrees(*grammars::aNbN(), std::string("aab")));
    REQUIRE(hasUniqueTree(*grammars::aNbN(), std::string("aabb")));
    REQUIRE(hasUniqueTree(*grammars::ccStar(), std::string("c")));
    REQUIRE(hasUniqueTree(*grammars::ccStar(), std::string("cccccc")));
    REQUIRE_FALSE(hasInfTrees(*grammars::ccStar(), std::string("cc")));
}

TEST_CASE("counting in ambiguous", "[counting, ambiguous]") {
    REQUIRE(hasInfTrees(*grammars::nestedParentheses(), std::string("[[]]")));
    REQUIRE(hasInfTrees(*grammars::nestedParentheses(), std::string("[()()]")));
    REQUIRE(hasNoTrees(*grammars::nestedParentheses(), std::string("[(]")));

    REQUIRE(hasNTrees(*grammars::ThreeSameWords(), std::string("aa"), 3));
    REQUIRE(hasNoTrees(*grammars::ThreeSameWords(), std::string("a")));

    REQUIRE(hasNTrees(*grammars::ABCInEightOptions(), std::string("abc"), 8));

    REQUIRE(hasNTrees(Alternation<AsciiAlphabet>(epsilon, epsilon), std::string(""), 2));
}

TEST_CASE("a^n b^n marked", "[counting, parse tree, unambiguous, non-regular]") {
    auto* grammar = grammars::aNbN_marked();
    REQUIRE(contains(*grammar, std::vector{1, 1, 1, 2, 2, 2}));
    REQUIRE(contains(*grammar, std::vector<int>{}));
    REQUIRE_FALSE(contains(*grammar, std::vector{1, 2, 2, 2, 3}));

    REQUIRE(hasNoTrees(*grammar, std::vector{1, 1, 2}));
    REQUIRE(hasUniqueTree(*grammar, std::vector{1, 1, 2, 2}));
    REQUIRE_FALSE(hasInfTrees(*grammar, std::vector{2}));

    REQUIRE(hasSuchParseTree(*grammar, std::vector<int>{}, std::vector{-2}));
    REQUIRE(hasSuchParseTree(*grammar, std::vector{1, 2}, std::vector{1, -2, 2, -1}));
    REQUIRE(hasSuchParseTree(*grammar, std::vector{1, 1, 2, 2}, std::vector{1, 1, -2, 2, -1, 2, -1}));
}

TEST_CASE("cc* marked", "[counting, parse tree, unambiguous, regular]") {
    auto* grammar = grammars::ccStar_marked();
    REQUIRE(hasUniqueTree(*grammar, std::vector{3}));
    REQUIRE(hasNoTrees(*grammar, std::vector<int>{}));
    REQUIRE(hasUniqueTree(*grammar, std::vector{3, 3, 3, 3, 3, 3, 3}));
    REQUIRE_FALSE(hasInfTrees(*grammar, std::vector{3, 3}));

    REQUIRE(hasSuchParseTree(*grammar, std::vector<int>{3}, std::vector{3, -2}));
    REQUIRE(hasSuchParseTree(*grammar, std::vector<int>{3, 3}, std::vector{3, -2, 3, -1}));
    REQUIRE(hasSuchParseTree(*grammar, std::vector<int>{3, 3, 3, 3}, std::vector{3, -2, 3, -1, 3, -1, 3, -1}));
}

TEST_CASE("ambiguous marked", "[counting, parse tree, ambiguous]") {
    auto* grammar = grammars::ABCInEightOptions_marked();
    REQUIRE(hasNTrees(*grammar, std::vector{1, 2, 3}, 8));
    REQUIRE(hasNoTrees(*grammar, std::vector{1}));

    grammar = grammars::nestedParentheses_marked();
    REQUIRE(hasInfTrees(*grammar, std::vector{3, 3, 4, 4}));
    REQUIRE(hasInfTrees(*grammar, std::vector{3, 1, 2, 1, 2, 4}));
    REQUIRE(hasNoTrees(*grammar, std::vector{3, 1, 4}));
}

TEST_CASE("catalan marked", "[counting, parse tree, ambiguous, finitely ambiguous]") {
    auto* grammar = grammars::catalan_marked();
    REQUIRE(hasNTrees(*grammar, std::vector{1, 1, 1}, 2));
    REQUIRE(hasNTrees(*grammar, std::vector(5, 1), 14));
    REQUIRE(hasNTrees(*grammar, std::vector(13, 1), 208012));
}
