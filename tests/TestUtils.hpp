#pragma once

#include <catch.hpp>
#include <Operators.hpp>
#include <Alphabets.hpp>
#include <Concepts.hpp>
#include <Parser.hpp>
#include <IntegerSemiring.hpp>
#include <BooleanSemiring.hpp>
#include <ParseTreeSemiring.hpp>

namespace grammars {
    const Node<AsciiAlphabet>* aNbN();
    const Node<AsciiAlphabet>* nestedParentheses();
    const Node<AsciiAlphabet>* ccStar();
    const Node<AsciiAlphabet>* ThreeSameWords();
    const Node<AsciiAlphabet>* ABCInEightOptions();

    const Node<IntAlphabet>* aNbN_marked();
    const Node<IntAlphabet>* ccStar_marked();
    const Node<IntAlphabet>* ABCInEightOptions_marked();
    const Node<IntAlphabet>* nestedParentheses_marked();
    const Node<IntAlphabet>* catalan_marked();
}

namespace tests {
    static const Token<AsciiAlphabet> a{'a'};
    static const ParsedToken<AsciiAlphabet> parsedA{'a'};
    static const Token<AsciiAlphabet> b{'b'};
    static const ParsedToken<AsciiAlphabet> parsedB{'b'};
    static const Token<AsciiAlphabet> c{'c'};
    static const ParsedToken<AsciiAlphabet> parsedC{'c'};
    static const Token<AsciiAlphabet> leftPar{'('};
    static const Token<AsciiAlphabet> rightPar{')'};
    static const Token<AsciiAlphabet> leftBracket{'['};
    static const Token<AsciiAlphabet> rightBracket{']'};
    static const Epsilon<AsciiAlphabet> epsilon;
    static const Empty<AsciiAlphabet>& empty = Empty<AsciiAlphabet>::empty;

    template<typename Sigma, typename Sequence>
    bool contains(const Node<Sigma>& grammar, const Sequence& word);

    template<typename Sigma, typename Sequence>
    bool hasNTrees(const Node<Sigma>& grammar, const Sequence& word, int N);

    template<typename Sigma, typename Sequence>
    bool hasInfTrees(const Node<Sigma>& grammar, const Sequence& word);

    template<typename Sigma, typename Sequence>
    bool hasUniqueTree(const Node<Sigma>& grammar, const Sequence& word) {
        return hasNTrees(grammar, word, 1);
    }

    template<typename Sigma, typename Sequence>
    bool hasNoTrees(const Node<Sigma>& grammar, const Sequence& word) {
        return !contains(grammar, word);
    }

    bool hasSuchParseTree(const Node<IntAlphabet>& grammar, const std::vector<int>& word,
                          const std::vector<int>& parseTree);

    /*******************************************************/
    /*******************************************************/

    template<typename Sigma, typename Sequence>
    bool contains(const Node<Sigma>& grammar, const Sequence& word) {
        Parser<Sigma> parser;
        bool booleanAns = BooleanSemiring::isZero(parser.doesContain(grammar, word));
        bool integerAns = IntegerSemiring::isZero(parser.countParseTrees(grammar, word));
        bool parseTreeAns = ParseTreeSemiring<typename Sigma::token_t>::isZero(parser.parse(grammar, word));

        assert(booleanAns == integerAns && integerAns == parseTreeAns);
        return !booleanAns;
    }

    template<typename Sigma, typename Sequence>
    bool hasNTrees(const Node<Sigma>& grammar, const Sequence& word, int N) {
        Parser<Sigma> parser;
        if (N == 0) return hasNoTrees(grammar, word);

        bool integerAns = parser.countParseTrees(grammar, word).getValue() == N;
        if (N == 1) {
            bool parseTreeAns = parser.parse(grammar, word).getQuantity() == UNIQUE;
            assert(parseTreeAns == integerAns);
        } else if (N > 1) {
            bool parseTreeAns = parser.parse(grammar, word).getQuantity() == FINITELY_MANY;
            assert(parseTreeAns == integerAns);
        }

        bool booleanAns = BooleanSemiring::isOne(parser.doesContain(grammar, word));
        assert(!integerAns || booleanAns);

        return integerAns;
    }

    template<typename Sigma, typename Sequence>
    bool hasInfTrees(const Node<Sigma>& grammar, const Sequence& word) {
        Parser<Sigma> parser;
        bool booleanAns = BooleanSemiring::isInfinite(parser.doesContain(grammar, word));
        bool integerAns = IntegerSemiring::isInfinite(parser.countParseTrees(grammar, word));
        bool parseTreeAns = ParseTreeSemiring<typename Sigma::token_t>::isInfinite(parser.parse(grammar, word));

        assert(!integerAns || booleanAns);
        assert(integerAns == parseTreeAns);
        return integerAns;
    }
}
