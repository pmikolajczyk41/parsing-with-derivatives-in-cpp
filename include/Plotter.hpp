#pragma once

#include <Nodes.hpp>
#include <unordered_set>
#include <sstream>
#include <cxxabi.h>
#include <regex>

class Plotter {
public:
    void saveDescription(const std::string& filename) const;
    template<typename Sigma>
    std::string generatePUMLDescription(const Node<Sigma>& root);
    void setPrettyFormatting() { prettyFormatting = true; }
    void setStandardFormatting() { prettyFormatting = false; }
    void reset() { description.clear(); }

private:
    std::string description;
    bool prettyFormatting = true;

    static std::string toString(const Nullable& nullable);

    template<typename Sigma>
    std::string getNameOf(const Node<Sigma>* node);
    template<typename Sigma>
    void markRoot(const Node<Sigma>& root);
    template<typename Sigma>
    void listNodes(std::unordered_set<const Node<Sigma>*> Nodes);
    template<typename Sigma>
    void linkNodes(std::unordered_set<const Node<Sigma>*> Nodes);
    void replaceInDescription(const std::regex& regex, const std::string& replacement);
    void addHeaderFooter();
    void prettyFormat();
};

/**************************************************************************/
/**************************************************************************/
/**************************************************************************/

template<typename Sigma>
void Plotter::listNodes(std::unordered_set<const Node<Sigma>*> Nodes) {
    std::ostringstream nodeDescription;
    for (auto* node : Nodes) {
        std::string nullable = toString(node->getNullableState());
        nodeDescription << "object \"" << getNameOf(node) << "\" as " << node << " "
                        << "{\nnullable = " << nullable << "\n"
                        << "address = " << node << "\n}\n";
    }
    description += nodeDescription.str() + "\n";
}

template<typename Sigma>
void Plotter::linkNodes(std::unordered_set<const Node<Sigma>*> Nodes) {
    std::ostringstream nodeDescription;
    for (auto* node : Nodes) {
        if (auto* binaryCast = dynamic_cast<const BinaryNode<Sigma>*>(node))
            nodeDescription << node << "-->" << &(binaryCast->getLeftChild()) << " :l \n"
                            << node << "-->" << &(binaryCast->getRightChild()) << " :r \n";
        else if(auto* deltaCast = dynamic_cast<const Delta<Sigma>*>(node))
            nodeDescription << node << "-->" << &(deltaCast->getRef()) << "\n";
    }
    description += nodeDescription.str() + "\n";
}

template<typename Sigma>
std::string Plotter::generatePUMLDescription(const Node<Sigma>& root) {
    std::unordered_set<const Node<Sigma>*> VisitedNodes;

    getAllNodes(root, VisitedNodes);
    listNodes(VisitedNodes);
    linkNodes(VisitedNodes);

    markRoot(root);
    if (prettyFormatting) prettyFormat();
    addHeaderFooter();
    return description;
}

template<typename Sigma>
std::string Plotter::getNameOf(const Node<Sigma>* node) {
    std::ostringstream os;
    if (auto* token = dynamic_cast<const Token<Sigma>*>(node))
        os << token->getToken();
    else if (auto* parsedToken = dynamic_cast<const ParsedToken<Sigma>*>(node))
        os << std::string("Epsilon_") << parsedToken->getToken();
    else {
        const std::type_info& ti = typeid(*node);
        os << abi::__cxa_demangle(ti.name(), 0, 0, nullptr);
    }
    return os.str();
}
template<typename Sigma>
void Plotter::markRoot(const Node<Sigma>& root) {
    std::ostringstream rootMarker;
    rootMarker << "\n" << &root << " <<root>>\n\n";
    description += rootMarker.str();
}
