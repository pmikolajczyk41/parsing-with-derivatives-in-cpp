#include <OnceSettable.hpp>

enum SymbolType {
    EPSILON, TERMINAL, NONTERMINAL, MARKER
};

template<typename ValueT = int>
class Symbol {
public:
    Symbol(const SymbolType& type, const ValueT& value) noexcept :
            type(type), value(value) {}

    SymbolType getType() const { return type; }
    ValueT getValue() const { return value; }

protected:
    const SymbolType type;
    const ValueT value;
};

template<typename ValueT = int>
class EpsilonSymbol : public Symbol<ValueT> {
public:
    EpsilonSymbol() noexcept : Symbol<ValueT>(EPSILON, 0) {}
};