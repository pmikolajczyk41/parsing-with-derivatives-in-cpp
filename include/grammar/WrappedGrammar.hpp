#pragma once

#include <vector>
#include <deque>
#include <memory>
#include <nodes/Nodes.hpp>
#include <Alphabets.hpp>
#include <Symbol.hpp>

using rule_t = std::deque<Symbol<int>>;
using ruleset_t = std::vector<rule_t>;
using productions_t = std::vector<ruleset_t>;

class WrappedGrammar {
public:
    explicit WrappedGrammar(productions_t, int= 0);

    const BinaryNode<IntAlphabet>* getRoot() const;

private:
    void verifyGrammar();
    void verifyProductions();
    void verifySymbols();
    void addMarkers();
    void createTerminals();
    void createNonterminals();
    void binarize();
    void verifyProduct();
    template<typename F>
    void forEverySymbolInP(F);
    void setChildFromSymbol(BinaryNode<IntAlphabet>*, Symbol<int>&,
                            void(BinaryNode<IntAlphabet>::*)(const Node<IntAlphabet>&));

    const int startSymbol;
    productions_t P;

    std::vector<std::unique_ptr<BinaryNode<IntAlphabet>>> Nonterminals;
    std::vector<Token<IntAlphabet>> Terminals;
    std::vector<ParsedToken<IntAlphabet>> Markers;
    Epsilon<IntAlphabet> epsilon{};

    struct RulesetToProcess {
        RulesetToProcess(ruleset_t ruleset, BinaryNode<IntAlphabet>* parent, bool isLeft, int indexOrNegative) :
                ruleset(std::move(ruleset)), parent(parent), isLeft(isLeft), indexOrNegative(indexOrNegative) {}

        ruleset_t ruleset;
        BinaryNode<IntAlphabet>* parent;
        bool isLeft;
        int indexOrNegative;
    };
};