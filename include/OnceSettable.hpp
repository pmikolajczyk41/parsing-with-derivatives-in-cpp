#pragma once

#include <stdexcept>

template<typename T>
class OnceSettable {
public:
    explicit OnceSettable(T value) : value(value) {}
    OnceSettable& operator=(const OnceSettable&) = delete;

    T& getValue() const { return value; }
    void setValue(T value) const;

private:
    mutable bool alreadySet = false;
    mutable T value;
};

template<typename T>
void OnceSettable<T>::setValue(T value) const {
    if (alreadySet) throw std::runtime_error("Value already set");

    alreadySet = true;
    this->value = value;
}