#pragma once

#include <Nodes.hpp>

template<typename Sigma>
const Node<Sigma>& operator|(const Node<Sigma>& a, const Node<Sigma>& b) { return *(new Alternation<Sigma>(a, b)); }

template<typename Sigma>
const Node<Sigma>& operator&(const Node<Sigma>& a, const Node<Sigma>& b) { return *(new Concatenation<Sigma>(a, b)); }

template<typename Sigma>
const Node<Sigma>& kleeneStar(const Node<Sigma>& node) {
    //L* := eps | ( L . L* )
    auto* result = new Alternation<Sigma>(*(new Epsilon<Sigma>{}), nullptr);
    auto* recursion = new Concatenation<Sigma>(node, *result);
    result->setRightChild(*recursion);
    return *result;
}

template<typename Sigma>
const Node<Sigma>& sequence(Sequence sequence) {
    size_t length = sequence.end() - sequence.begin();
    if (length == 0) return *(new Epsilon<Sigma>{});
    if (length == 1) return *(new Token<Sigma>(*(sequence.begin())));

    auto* firstToken = new Token<Sigma>(*(sequence.begin()));
    auto* secondToken = new Token<Sigma>(*(sequence.begin() + 1));
    auto result = &(*firstToken & *secondToken);
    for (int idx = 2; idx < length; idx++) {
        auto nextToken = new Token<Sigma>(*(sequence.begin() + idx));
        result = &(*result & *nextToken);
    }
    return *result;
}