#pragma once

#include <ostream>

struct IntegerElement {
    IntegerElement() : IntegerElement(0) {}
    explicit IntegerElement(int value) noexcept : value(value) {}
    explicit IntegerElement(const void* vptr) noexcept:
            IntegerElement(int(vptr != nullptr)) {}

    IntegerElement& operator+=(const IntegerElement&);
    IntegerElement& operator*=(const IntegerElement&);

    friend IntegerElement operator+(IntegerElement, const IntegerElement&);
    friend IntegerElement operator*(IntegerElement, const IntegerElement&);

    friend std::ostream& operator<<(std::ostream&, const IntegerElement&);

    bool isZero() { return value == 0; }
    bool isOne() { return value == 1; }
    bool isInfinite() { return value == -1; }

    int getValue() const { return value; }

private:
    int value;
};


struct IntegerSemiring {
    using element_t = IntegerElement;

    static element_t zero() { return ZERO; }
    static element_t one() { return ONE; }
    static element_t inf() { return INF; }

    static bool isZero(element_t e) { return e.isZero(); }
    static bool isOne(element_t e) { return e.isOne(); }
    static bool isInfinite(element_t e) { return e.isInfinite(); }

private:
    static IntegerElement ZERO;
    static IntegerElement ONE;
    static IntegerElement INF;
};