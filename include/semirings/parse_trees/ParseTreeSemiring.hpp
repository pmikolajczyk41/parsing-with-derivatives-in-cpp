#pragma once

#include <ostream>
#include <vector>
#include <ParseTreeElement.hpp>

template<typename NodeT>
struct ParseTreeSemiring {
    using element_t = ParseTreeElement<NodeT>;

    static element_t zero() { return ZERO; }
    static element_t one() { return ONE; }
    static element_t inf() { return INF; }

    static bool isZero(element_t e) { return e.isZero(); }
    static bool isOne(element_t e) { return e.isOne(); }
    static bool isInfinite(element_t e) { return e.isInfinite(); }

private:
    static element_t ZERO;
    static element_t ONE;
    static element_t INF;
};

template<typename NodeT>
typename ParseTreeSemiring<NodeT>::element_t
        ParseTreeSemiring<NodeT>::ZERO = std::move(ParseTreeElement<NodeT>::getEmpty());

template<typename NodeT>
typename ParseTreeSemiring<NodeT>::element_t
        ParseTreeSemiring<NodeT>::ONE = std::move(ParseTreeElement<NodeT>::getOne());

template<typename NodeT>
typename ParseTreeSemiring<NodeT>::element_t
        ParseTreeSemiring<NodeT>::INF = std::move(ParseTreeElement<NodeT>::getInfinity());
