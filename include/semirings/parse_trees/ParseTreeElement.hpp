#pragma once

#include <ostream>
#include <vector>
#include "TreeFragments.hpp"

enum Quantity {
    EMPTY = 0, UNIQUE = 1, FINITELY_MANY = 2, INFINITE = 3
};

template<typename TokenT>
class ParseTreeElement {
public:
    ParseTreeElement() noexcept : ParseTreeElement<TokenT>(EMPTY) {}
    explicit ParseTreeElement(const void* token) noexcept :
            ParseTreeElement<TokenT>(UNIQUE, static_cast<const TokenT*>(token)) {}

    static auto getEmpty() noexcept { return ParseTreeElement<TokenT>(); }
    static auto getOne() noexcept { return ParseTreeElement<TokenT>(UNIQUE); }
    static auto getInfinity() noexcept { return ParseTreeElement<TokenT>(INFINITE); }

    Quantity getQuantity() const { return quantity; }
    void traverseTree(std::vector<TokenT>& record) const {
        if (quantity == UNIQUE) treeFragment->traverseTree(record);
    }

    bool isZero() const { return quantity == EMPTY; }
    bool isOne() const {
        return quantity == UNIQUE && dynamic_cast<const AtomicFragment<TokenT>*>(treeFragment.get());
    }
    bool isInfinite() const { return quantity == INFINITE; }

    template<typename T>
    friend ParseTreeElement<T> operator+(const ParseTreeElement<T>&, const ParseTreeElement<T>&);
    template<typename T>
    friend ParseTreeElement<T> operator*(const ParseTreeElement<T>&, const ParseTreeElement<T>&);

    template<typename T>
    friend std::ostream& operator<<(std::ostream&, const ParseTreeElement<T>&);

private:
    explicit ParseTreeElement(Quantity quantity, const TokenT* token = nullptr) : quantity(quantity) {
        if (token != nullptr && quantity == UNIQUE) {
            treeFragment.reset(new AtomicFragment<TokenT>(*token));
        };
    }

    Quantity quantity;
    std::shared_ptr<const TreeFragment<TokenT>> treeFragment;
};

template<typename TokenT>
ParseTreeElement<TokenT> operator+(const ParseTreeElement<TokenT>& lhs, const ParseTreeElement<TokenT>& rhs) {
    ParseTreeElement<TokenT> result;
    if (lhs.quantity == rhs.quantity && lhs.quantity == UNIQUE)
        result.quantity = FINITELY_MANY;
    else result.quantity = std::max(lhs.quantity, rhs.quantity);

    if (result.quantity == UNIQUE && rhs.quantity == UNIQUE) result.treeFragment = rhs.treeFragment;
    else if (result.quantity == UNIQUE) result.treeFragment = lhs.treeFragment;

    return result;
}

template<typename TokenT>
ParseTreeElement<TokenT> operator*(const ParseTreeElement<TokenT>& lhs, const ParseTreeElement<TokenT>& rhs) {
    ParseTreeElement<TokenT> result;
    if (lhs.quantity != EMPTY && rhs.quantity != EMPTY) {
        result.quantity = std::max(lhs.quantity, rhs.quantity);

        if (result.quantity == UNIQUE)
            result.treeFragment.reset(new ConcatFragment<TokenT>(lhs.treeFragment, rhs.treeFragment));
    }
    return result;
}

template<typename TokenT>
std::ostream& operator<<(std::ostream& os, const ParseTreeElement<TokenT>& element) {
    if (element.quantity == EMPTY) os << "empty parse tree";
    else if (element.quantity == INFINITE) os << "infinitely many parse trees";
    else if (element.quantity == FINITELY_MANY) os << "finitely many parse trees";
    else {
        std::vector<TokenT> result;
        element.traverseTree(result);
        for (auto& n : result)
            os << n << " ";
    }
    return os;
}
