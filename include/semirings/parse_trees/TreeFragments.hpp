#pragma once

#include <vector>
#include <memory>

template<typename TokenT>
class TreeFragment {
public:
    virtual void traverseTree(std::vector<TokenT>&) const = 0;
};

template<typename TokenT>
class AtomicFragment : public TreeFragment<TokenT> {
public:
    explicit AtomicFragment(TokenT token) : token(std::move(token)) {}
    TokenT getToken() const { return token; };
    void traverseTree(std::vector<TokenT>& record) const override { record.push_back(token); }

private:
    const TokenT token;
};

template<typename TokenT>
class ConcatFragment : public TreeFragment<TokenT> {
public:
    ConcatFragment(const std::shared_ptr<const TreeFragment<TokenT>>& left,
                   const std::shared_ptr<const TreeFragment<TokenT>>& right) : left(left), right(right) {}

    void traverseTree(std::vector<TokenT>& record) const override {
        if (left.get()) left->traverseTree(record);
        if (right.get()) right->traverseTree(record);
    }

private:
    std::shared_ptr<const TreeFragment<TokenT>> left;
    std::shared_ptr<const TreeFragment<TokenT>> right;
};
