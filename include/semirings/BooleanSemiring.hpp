#pragma once

struct BooleanSemiring {
    using element_t = bool;

    static element_t zero() { return false; }
    static element_t one() { return true; }
    static element_t inf() { return true; }

    static bool isZero(element_t e) { return !e; }
    static bool isOne(element_t e) { return e; }
    static bool isInfinite(element_t e) { return e; }
};
