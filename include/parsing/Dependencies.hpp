#pragma once

#include <nodes/Node.hpp>
#include <unordered_map>

template<typename Sigma>
struct dependency {
    dependency(const Node<Sigma>* parent, const Node<Sigma>* brother) : parent(parent), brother(brother) {}
    const Node<Sigma>* parent;
    const Node<Sigma>* brother;
};

template<typename Sigma, template<typename ...> typename Container = std::vector>
using dependencies_t = Container<dependency<Sigma>>;

template<typename Sigma>
using node_dep_map_t = std::unordered_map<const Node<Sigma>*, dependencies_t<Sigma>>;
