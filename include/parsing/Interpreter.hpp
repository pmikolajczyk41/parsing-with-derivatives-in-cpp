#pragma once

#include <Nodes.hpp>
#include <Dependencies.hpp>
#include <Concepts.hpp>
#include <unordered_set>
#include <unordered_map>
#include <queue>

template<Alphabet Sigma, Semiring R>
class Interpreter {
public:
    typename R::element_t interpretGrammar(const Node<Sigma>* root) {
        this->root = root;
        prepare();
        computeZeros();
        computeFinites();
        computeInfinities();
        return ComputedValues[root];
    }

private:
    using values_t = std::unordered_map<const Node<Sigma>*, typename R::element_t>;
    using node_set_t = std::unordered_set<const Node<Sigma>*>;

    void prepare();
    void computeZeros();
    void computeFinites();
    void computeInfinities();
    void markBasic();
    void propagateFinites();
    void generateDependencies();

    const Node<Sigma>* root;
    node_set_t Nodes;
    values_t ComputedValues;
    node_dep_map_t<Sigma> Dependencies;
};

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::prepare() {
    getAllNodes(*root, Nodes);
}

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::computeZeros() {
    computeNullability(*root);
    for (auto* node : Nodes)
        if (node->getNullableState() == NOT_NULLABLE)
            ComputedValues[node] = R::zero();
}

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::computeInfinities() {
    for (auto* node : Nodes)
        if (ComputedValues.find(node) == ComputedValues.end())
            ComputedValues[node] = R::inf();
}

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::computeFinites() {
    markBasic();
    generateDependencies();
    propagateFinites();
}

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::markBasic() {
    for (auto* node : Nodes) {
        if (dynamic_cast<const Epsilon<Sigma>*>(node))
            ComputedValues[node] = R::one();
        else if (auto* parsedTokenCast = dynamic_cast<const ParsedToken<Sigma>*>(node))
            ComputedValues[node] = typename R::element_t{&(parsedTokenCast->getToken())};
    }
}

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::generateDependencies() {
    for (auto* node : Nodes) {
        if (ComputedValues.count(node)) continue;
        else if (auto* binaryCast = dynamic_cast<const BinaryNode<Sigma>*>(node)) {
            auto* leftChild = &(binaryCast->getLeftChild());
            auto* rightChild = &(binaryCast->getRightChild());
            Dependencies[leftChild].emplace_back(node, rightChild);
            Dependencies[rightChild].emplace_back(node, leftChild);
        } else if (auto* deltaCast = dynamic_cast<const Delta<Sigma>*>(node)) {
            auto* ref = &(deltaCast->getRef());
            Dependencies[ref].emplace_back(node, nullptr);
        }
    }
}

template<typename Sigma, typename R>
void Interpreter<Sigma, R>::propagateFinites() {
    std::queue<const Node<Sigma>*> Todo;
    for (auto* node : Nodes)
        if (ComputedValues.count(node)) Todo.push(node);

    while (!Todo.empty()) {
        auto* node = Todo.front();
        Todo.pop();

        for (auto& d : Dependencies[node]) {
            const Node<Sigma>* p = d.parent;
            if (ComputedValues.count(p)) continue;
            else if (d.brother == nullptr) {
                ComputedValues[p] = ComputedValues[node];
                Todo.push(p);
            } else if (ComputedValues.count(d.brother)) {
                auto* binCast = dynamic_cast<const BinaryNode<Sigma>*>(p);
                bool isLeft = &(binCast->getLeftChild()) == node;

                if (auto* altCast = dynamic_cast<const Alternation<Sigma>*>(p)) {
                    if(isLeft) ComputedValues[altCast] = ComputedValues[node] + ComputedValues[d.brother];
                    else ComputedValues[altCast] = ComputedValues[d.brother] + ComputedValues[node];
                }
                else if (auto* conCast = dynamic_cast<const Concatenation<Sigma>*>(p)){
                    if(isLeft) ComputedValues[conCast] = ComputedValues[node] * ComputedValues[d.brother];
                    else ComputedValues[conCast] = ComputedValues[d.brother] * ComputedValues[node];
                }
                Todo.push(p);
            }
        }
    }
}
