#pragma once

#include <Interpreter.hpp>
#include <IntegerSemiring.hpp>
#include <ParseTreeSemiring.hpp>
#include <BooleanSemiring.hpp>

template<typename Sigma>
class Parser {
    using token_t = typename Sigma::token_t;

public:
    bool doesContain(const Node<Sigma>& grammar, Sequence word) const;
    IntegerElement countParseTrees(const Node<Sigma>& grammar, Sequence word) const;
    ParseTreeElement<token_t> parse(const Node<Sigma>& grammar, Sequence word) const;

private:
    template<Semiring R>
    typename R::element_t handleGrammar(const Node<Sigma>& grammar, Sequence word) const;
    const Node<Sigma>* takeDerivativesOver(const Node<Sigma>& grammar, Sequence word) const;
};

template<typename Sigma>
const Node<Sigma>* Parser<Sigma>::takeDerivativesOver(const Node<Sigma>& grammar, Sequence word) const {
    auto* currentGrammar = &grammar;
    for (token_t c : word)
        currentGrammar = &(currentGrammar->takeDerivative(c));
    return currentGrammar;
}

template<typename Sigma>
bool Parser<Sigma>::doesContain(const Node<Sigma>& grammar, Sequence word) const {
    return handleGrammar<BooleanSemiring>(grammar, word);
}

template<typename Sigma>
IntegerElement Parser<Sigma>::countParseTrees(const Node<Sigma>& grammar, Sequence word) const {
    return handleGrammar<IntegerSemiring>(grammar, word);
}

template<typename Sigma>
ParseTreeElement<typename Sigma::token_t> Parser<Sigma>::parse(const Node<Sigma>& grammar, Sequence word) const {
    return handleGrammar<ParseTreeSemiring<token_t>>(grammar, word);
}

template<typename Sigma>
template<Semiring R>
typename R::element_t Parser<Sigma>::handleGrammar(const Node<Sigma>& grammar, Sequence word) const {
    auto* processedGrammar = takeDerivativesOver(grammar, word);
    Interpreter<Sigma, R> interpreter;
    return interpreter.interpretGrammar(processedGrammar);
}
