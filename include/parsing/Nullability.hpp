#pragma once

#include <unordered_set>
#include <queue>
#include <algorithm>
#include <nodes/Nodes.hpp>
#include <parsing/Dependencies.hpp>

namespace {
    template<typename Sigma>
    using node_set_t = std::unordered_set<const Node<Sigma>*>;

    template<typename Sigma>
    void generateDependencies(node_set_t<Sigma>& Nodes, node_dep_map_t<Sigma>& Dependencies) {
        static const auto* placeholder = new Epsilon<Sigma>{};
        for (auto* node : Nodes) {
            if (node->getNullableState() != UNKNOWN) continue;
            if (auto* alternationCast = dynamic_cast<const Alternation<Sigma>*>(node)) {
                auto* leftChild = &(alternationCast->getLeftChild());
                auto* rightChild = &(alternationCast->getRightChild());
                Dependencies[leftChild].emplace_back(node, placeholder);
                Dependencies[rightChild].emplace_back(node, placeholder);
            } else if (auto* concatenationCast = dynamic_cast<const Concatenation<Sigma>*>(node)) {
                auto* leftChild = &(concatenationCast->getLeftChild());
                auto* rightChild = &(concatenationCast->getRightChild());
                Dependencies[leftChild].emplace_back(node, rightChild);
                Dependencies[rightChild].emplace_back(node, leftChild);
            } else if (auto* deltaCast = dynamic_cast<const Delta<Sigma>*>(node)) {
                auto* ref = &(deltaCast->getRef());
                Dependencies[ref].emplace_back(node, placeholder);
            }
        }
    }
}

template<typename Sigma>
void computeNullability(const Node<Sigma>& grammar) {
    node_set_t<Sigma> Nodes;
    node_dep_map_t<Sigma> Dependencies;

    getAllNodes(grammar, Nodes);
    generateDependencies(Nodes, Dependencies);

    std::queue<const Node<Sigma>*> Todo;
    for (auto* node : Nodes)
        if (node->getNullableState() == NULLABLE) Todo.push(node);

    while (!Todo.empty()) {
        auto node = Todo.front();
        Todo.pop();

        for (auto& d : Dependencies[node])
            if (d.parent->getNullableState() == UNKNOWN &&
                d.brother->getNullableState() == NULLABLE) {
                d.parent->setNullableState(NULLABLE);
                Todo.push(d.parent);
            }
    }

    for (auto* node : Nodes)
        if (node->getNullableState() == UNKNOWN) node->setNullableState(NOT_NULLABLE);
}
