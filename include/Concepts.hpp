#pragma once

template<typename T>
concept bool Sequence =
        requires(T t) {
            { t.begin() } -> typename T::iterator;
            { t.end() } -> typename T::iterator;
            requires std::is_same_v<typename T::value_type, typename T::iterator::value_type>;
};

template<typename T, typename U = void*>
concept bool Alphabet =
        requires (typename T::token_t a, typename T::token_t b,
            typename T::token_t&& key, typename T::template map_t<U> map) {
            { a == b } -> bool;
            { a != b } -> bool;
            { map[key] } -> U&;
};

template<typename T>
concept bool Semiring =
        requires(typename T::element_t a, typename T::element_t b, const void* vptr) {

            { a + b } -> typename T::element_t;
            { a * b } -> typename T::element_t;

            { T::zero() } -> typename T::element_t;
            { T::one() } -> typename T::element_t;
            { T::inf() } -> typename T::element_t;

            { typename T::element_t{vptr} };
            { typename T::element_t{} };    //for use as a value in hash maps

            { T::isZero(a) } -> bool;
            { T::isOne(a) } -> bool;
            { T::isInfinite(a) } -> bool;
};