#pragma once

#include <unordered_map>

struct AsciiAlphabet {
    using token_t = char;

    template<typename T>
    using map_t = std::unordered_map<token_t, T>;
};

struct IntAlphabet {
    using token_t = int;

    template<typename T>
    using map_t = std::unordered_map<int, T>;
};
