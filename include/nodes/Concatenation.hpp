#pragma once

#include <BinaryNode.hpp>
#include <Nullability.hpp>

template<typename Sigma>
class Concatenation : public BinaryNode<Sigma> {
public:
    using token_t = typename Sigma::token_t;

    Concatenation(const Node<Sigma>& leftChild, const Node<Sigma>& rightChild) {
        this->setLeftChild(leftChild);
        this->setRightChild(rightChild);
    }
    Concatenation(const Node<Sigma>& leftChild, nullptr_t) { this->setLeftChild(leftChild); }
    Concatenation(nullptr_t, const Node<Sigma>& rightChild) { this->setRightChild(rightChild); }
    Concatenation(nullptr_t, nullptr_t) {}

    const Node<Sigma>& takeDerivative(token_t) const override;
};


template<typename Sigma>
const Node<Sigma>& Concatenation<Sigma>::takeDerivative(token_t c) const {
    Node<Sigma>** derivative = &(this->derivatives[c]);
    if (*derivative == nullptr) {
        auto leftNullability = this->getLeftChild().getNullableState();
        if (leftNullability == UNKNOWN)
            computeNullability(this->getLeftChild());
        bool leftChildNullable = this->getLeftChild().getNullableState() == NULLABLE;

        if (leftChildNullable) {
            auto* leftConcat = new Concatenation<Sigma>(nullptr, this->getRightChild());
            auto* rightConcat = new Concatenation<Sigma>(*(new Delta<Sigma>{this->getLeftChild()}), nullptr);
            *derivative = new Alternation<Sigma>(*leftConcat, *rightConcat);

            leftConcat->setLeftChild(this->getLeftChild().takeDerivative(c));
            rightConcat->setRightChild(this->getRightChild().takeDerivative(c));
        } else {
            *derivative = new Concatenation<Sigma>(nullptr, this->getRightChild());
            static_cast<BinaryNode<Sigma>*>(*derivative)->setLeftChild(this->getLeftChild().takeDerivative(c));
        }
    }
    return **derivative;
}
