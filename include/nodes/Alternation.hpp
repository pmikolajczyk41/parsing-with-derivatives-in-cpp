#pragma once

#include <BinaryNode.hpp>

template<typename Sigma>
class Alternation : public BinaryNode<Sigma> {
public:
    using token_t = typename Sigma::token_t;

    Alternation(const Node<Sigma>& leftChild, const Node<Sigma>& rightChild) {
        this->setLeftChild(leftChild);
        this->setRightChild(rightChild);
    }
    Alternation(const Node<Sigma>& leftChild, nullptr_t) { this->setLeftChild(leftChild); }
    Alternation(nullptr_t, const Node<Sigma>& rightChild) { this->setRightChild(rightChild); }
    Alternation(nullptr_t, nullptr_t) {}

    const Node<Sigma>& takeDerivative(token_t c) const override;
};


template<typename Sigma>
const Node<Sigma>& Alternation<Sigma>::takeDerivative(token_t c) const {
    Node<Sigma>** derivative = &(this->derivatives[c]);
    if (*derivative == nullptr) {
        auto* alternation = new Alternation<Sigma>(nullptr, nullptr);
        *derivative = alternation;
        alternation->setLeftChild(this->leftChild.getValue()->takeDerivative(c));
        alternation->setRightChild(this->rightChild.getValue()->takeDerivative(c));
    }
    return **derivative;
}
