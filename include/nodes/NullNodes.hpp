#pragma once

#include <Node.hpp>

template<typename Sigma>
class NullNode : public Node<Sigma> {
public:
    using token_t = typename Sigma::token_t;

    const Node<Sigma>& takeDerivative(token_t c) const override { return Empty<Sigma>::empty; }
    virtual ~NullNode() = 0;
};

template<typename Sigma>
NullNode<Sigma>::~NullNode() {}

template<typename Sigma>
class Epsilon : public NullNode<Sigma> {
public:
    Epsilon() { this->setNullableState(NULLABLE); }
    ~Epsilon() override = default;
};

template<typename Sigma>
class ParsedToken : public NullNode<Sigma> {
public:
    using token_t = typename Sigma::token_t;

    explicit ParsedToken(token_t token) : token(token) { this->setNullableState(NULLABLE); }
    const token_t& getToken() const { return token; }
    ~ParsedToken() override = default;

private:
    const token_t token;
};

template<typename Sigma>
class Delta : public NullNode<Sigma> {
public:
    using token_t = typename Sigma::token_t;

    explicit Delta(const Node<Sigma>& node) : ref(node) { this->setNullableState(NULLABLE); }
    const Node<Sigma>& getRef() const { return ref; }
    ~Delta() override = default;

private:
    const Node<Sigma>& ref;
};
