#pragma once

#include <Node.hpp>

template<typename Sigma>
class Token : public Node<Sigma> {
public:
    using token_t = typename Sigma::token_t;

    explicit Token(token_t c) : token(c) { this->setNullableState(NOT_NULLABLE); }

    const token_t& getToken() const { return token; }
    const Node<Sigma>& takeDerivative(token_t c) const override {
        if (c == token) return *(new ParsedToken<Sigma>{c});
        return Empty<Sigma>::empty;
    }

private:
    const token_t token;
};
