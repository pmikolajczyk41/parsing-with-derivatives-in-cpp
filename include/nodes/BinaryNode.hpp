#pragma once

#include <Node.hpp>

template<typename Sigma>
class BinaryNode : public Node<Sigma> {
public:
    using map_t = typename Sigma::template map_t<Node<Sigma>*>;

    void setLeftChild(const Node<Sigma>& child) { this->leftChild.setValue(&child); }
    void setRightChild(const Node<Sigma>& child) { this->rightChild.setValue(&child); }
    const Node<Sigma>& getLeftChild() const { return *(leftChild.getValue()); }
    const Node<Sigma>& getRightChild() const { return *(rightChild.getValue()); }

private:
    struct UninitializedChild;
    UninitializedChild uninitializedChild;

protected:
    const OnceSettable<const Node<Sigma>*> leftChild{&uninitializedChild};
    const OnceSettable<const Node<Sigma>*> rightChild{&uninitializedChild};
    mutable map_t derivatives;
};

template<typename Sigma>
struct BinaryNode<Sigma>::UninitializedChild : public Node<Sigma> {
    const Node<Sigma>& takeDerivative(typename Sigma::token_t c) const { throw std::runtime_error(message); }
    Nullable getNullableState() const { throw std::runtime_error(message); }
    void setNullableState(Nullable) const { throw std::runtime_error(message); }

private:
    const char* message = "Uninitialized child";
};