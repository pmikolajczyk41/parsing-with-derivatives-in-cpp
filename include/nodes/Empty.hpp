#pragma once

#include <Node.hpp>

template<typename Sigma>
class Empty : public Node<Sigma> {
public:
    static const Empty<Sigma> empty;
    using token_t = typename Sigma::token_t;

    const Node<Sigma>& takeDerivative(token_t) const override { return empty; }

private:
    Empty() { this->setNullableState(NOT_NULLABLE); }
};

template<typename Sigma>
const Empty<Sigma> Empty<Sigma>::empty{};
