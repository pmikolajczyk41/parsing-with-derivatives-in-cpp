#pragma once

#define USE_CONCEPTS

#include <OnceSettable.hpp>
#include <Concepts.hpp>

enum Nullable {
    UNKNOWN = -1, NOT_NULLABLE = 0, NULLABLE = 1
};


template<typename Sigma>
#ifdef USE_CONCEPTS
requires Alphabet<Sigma>
#endif
class Node {
public:
    using token_t = typename Sigma::token_t;

    explicit Node(Nullable n = UNKNOWN) : nullable(n) {}

    Nullable getNullableState() const { return nullable.getValue(); }
    void setNullableState(Nullable n) const { this->nullable.setValue(n); }

    virtual const Node& takeDerivative(token_t) const = 0;

protected:
    mutable OnceSettable<Nullable> nullable;
};


template<typename Sigma>
class Empty;

template<typename Sigma>
class Epsilon;

template<typename Sigma>
class ParsedToken;

template<typename Sigma>
class Delta;

template<typename Sigma>
class Token;

template<typename Sigma>
class BinaryNode;

template<typename Sigma>
class Concatenation;

template<typename Sigma>
class Alternation;

template<typename Sigma>
void computeNullability(const Node<Sigma>& grammar);

template<typename Sigma, template<typename ...> typename Container>
void getAllNodes(const Node<Sigma>& node, Container<const Node<Sigma>*>& VisitedNodes) {
    if (VisitedNodes.count(&node)) return;
    VisitedNodes.insert(&node);
    if (auto* binaryCast = dynamic_cast<const BinaryNode<Sigma>*>(&node)) {
        getAllNodes(binaryCast->getLeftChild(), VisitedNodes);
        getAllNodes(binaryCast->getRightChild(), VisitedNodes);
    } else if (auto* deltaCast = dynamic_cast<const Delta<Sigma>*>(&node))
        getAllNodes(deltaCast->getRef(), VisitedNodes);
}
