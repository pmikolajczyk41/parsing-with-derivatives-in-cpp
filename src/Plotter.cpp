#include <Plotter.hpp>
#include <Operators.hpp>
#include <regex>
#include <fstream>

std::string Plotter::toString(const Nullable& nullable) {
    if (nullable == NOT_NULLABLE) return "NOT_NULLABLE";
    if (nullable == NULLABLE) return "NULLABLE";
    return "UNKNOWN";
}

void Plotter::saveDescription(const std::string& filename) const {
    std::ofstream ofstream;
    ofstream.open(filename);
    ofstream << description;
    ofstream.close();
}

void Plotter::addHeaderFooter() {
    std::string header = "@startuml\n";
    std::string footer = "@enduml\n";
    description = header + description + footer;
}

void Plotter::replaceInDescription(const std::regex& regex, const std::string& replacement) {
    description = std::regex_replace(description, regex, replacement);
}

void Plotter::prettyFormat() {
    replaceInDescription(std::regex("<\\w*>(?!>)"), ""); //cover <template>, but not <<root>> marker
    replaceInDescription(std::regex("Alternation"), "⋃");
    replaceInDescription(std::regex("Concatenation"), "⨀");
    replaceInDescription(std::regex("Empty"), "∅");
    replaceInDescription(std::regex("Epsilon"), "ε");
    replaceInDescription(std::regex("Delta"), "δ");
}
