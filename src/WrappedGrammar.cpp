#include <WrappedGrammar.hpp>
#include <cassert>

WrappedGrammar::WrappedGrammar(productions_t P, const int startSymbol) :
        startSymbol(startSymbol), P(std::move(P)) {
    verifyGrammar();
    addMarkers();
    createTerminals();
    createNonterminals();
    binarize();
    verifyProduct();
}

template<typename F>
void WrappedGrammar::forEverySymbolInP(F lambda) {
    for (auto& ruleset : P)
        for (auto& rule: ruleset)
            for (auto& symbol : rule)
                lambda(symbol);
}

void WrappedGrammar::verifyGrammar() {
    verifyProductions();
    verifySymbols();
}

void WrappedGrammar::verifyProductions() {
    for (auto& ruleset: P) {
        assert(!ruleset.empty());
        for (auto& rule : ruleset)
            assert(!rule.empty());
    }
}

void WrappedGrammar::verifySymbols() {
    forEverySymbolInP([this](auto& symbol) {
        assert(symbol.getValue() >= 0);
        assert(symbol.getType() != MARKER);
        if (symbol.getType() == TERMINAL)
            assert(symbol.getValue() > 0);
        if (symbol.getType() == NONTERMINAL)
            assert(symbol.getValue() < P.size());
    });
}

void WrappedGrammar::addMarkers() {
    Markers.emplace_back(0); //align
    int productionCounter = 1;
    for (auto& ruleset : P)
        for (auto& rule : ruleset) {
            rule.emplace_back(MARKER, productionCounter);
            Markers.emplace_back(-(productionCounter++));
        }
}

void WrappedGrammar::createTerminals() {
    int maxTerminal = 0;
    forEverySymbolInP([&maxTerminal](auto& s) {
        if (s.getType() == TERMINAL)
            maxTerminal = std::max(maxTerminal, s.getValue());
    });
    for (int terminalId = 0; terminalId <= maxTerminal; ++terminalId)
        Terminals.emplace_back(terminalId);
}

void WrappedGrammar::createNonterminals() {
    for (auto& ruleset: P) {
        if (ruleset.size() == 1)
            Nonterminals.emplace_back(new Concatenation<IntAlphabet>(nullptr, nullptr));
        else Nonterminals.emplace_back(new Alternation<IntAlphabet>(nullptr, nullptr));
    }
}

void WrappedGrammar::binarize() {
    std::queue<RulesetToProcess> Todo;
    for (int id = 0; id < P.size(); ++id)
        Todo.emplace(ruleset_t(P[id]), nullptr, false, id);

    while (!Todo.empty()) {
        auto[ruleset, parent, isLeft, indexOrNegative] = std::move(Todo.front());
        Todo.pop();
        BinaryNode<IntAlphabet>* current;

        if (indexOrNegative >= 0) current = Nonterminals[indexOrNegative].get();
        else {
            if (ruleset.size() > 1)
                Nonterminals.emplace_back(new Alternation<IntAlphabet>(nullptr, nullptr));
            else Nonterminals.emplace_back(new Concatenation<IntAlphabet>(nullptr, nullptr));
            current = Nonterminals.back().get();
        }

        if (ruleset.size() > 1) {
            Todo.emplace(ruleset_t{ruleset.back()}, current, false, -1);
            ruleset.pop_back();
            Todo.emplace(ruleset, current, true, -1);
        } else {
            rule_t& rule = ruleset[0];
            assert(rule.size() > 1);
            assert(rule.front().getType() != MARKER);

            setChildFromSymbol(current, rule.front(), &BinaryNode<IntAlphabet>::setLeftChild);

            if (rule.size() == 2)
                setChildFromSymbol(current, rule.back(), &BinaryNode<IntAlphabet>::setRightChild);
            else {
                rule.pop_front();
                Todo.emplace(ruleset, current, false, -1);
            }
        }

        if (parent && isLeft) parent->setLeftChild(*current);
        else if (parent) parent->setRightChild(*current);
    }
}

void WrappedGrammar::setChildFromSymbol(BinaryNode<IntAlphabet>* parent, Symbol<int>& symbol,
                                        void(BinaryNode<IntAlphabet>::* setChild)(const Node<IntAlphabet>&)) {
    auto type = symbol.getType();
    auto value = symbol.getValue();

    if (type == MARKER) (parent->*setChild)(Markers[value]);
    else if (type == TERMINAL) (parent->*setChild)(Terminals[value]);
    else if (type == EPSILON) (parent->*setChild)(epsilon);
    else (parent->*setChild)(*Nonterminals[value]);
}

void WrappedGrammar::verifyProduct() {
    for (auto& nonterminal : Nonterminals) {
        nonterminal.get()->getLeftChild();
        nonterminal.get()->getRightChild();
    }
}

const BinaryNode<IntAlphabet>* WrappedGrammar::getRoot() const { return Nonterminals[startSymbol].get(); }
