#include <IntegerSemiring.hpp>

std::ostream& operator<<(std::ostream& os, const IntegerElement& element) {
    if (element.value < 0) os << "∞";
    else os << element.value;
    return os;
}

IntegerElement& IntegerElement::operator+=(const IntegerElement& rhs) {
    if (value < 0 || rhs.value < 0) value = -1;
    else value += rhs.value;
    return *this;
}

IntegerElement& IntegerElement::operator*=(const IntegerElement& rhs) {
    if (rhs.value == 0 || value == 0 ) value = 0;
    else if (value < 0 || rhs.value < 0) value = -1;
    else value *= rhs.value;
    return *this;
}

IntegerElement operator+(IntegerElement lhs, const IntegerElement& rhs) {
    lhs += rhs;
    return lhs;
}

IntegerElement operator*(IntegerElement lhs, const IntegerElement& rhs) {
    lhs *= rhs;
    return lhs;
}

IntegerSemiring::element_t IntegerSemiring::ZERO{0};
IntegerSemiring::element_t IntegerSemiring::ONE{1};
IntegerSemiring::element_t IntegerSemiring::INF{-1};
